## source("make-subsets.R")
## source("isomorphisms.R")
## library(numDeriv)

###############################################################################
## GENERATE OUTPUTS BY MONTE CARLO SAMPLING FROM A MODEL AND A TABLE OF SUBSETS
###############################################################################

## model: a real function taking a vector as argument 

## sizeMonteCarlo: number of explorations in the Monte Carlo experiment.
## One exploration consists of drawing at random a point in (0,1)^d from
## a product uniform distribution.

## keepIn: parameter of the Bernoulli-distributed random weights W.
## That is, P(W=1) = keepIn.

## dimension: number of inputs of the model; also denoted by 'd' throughout.

## subsets: it can be either one of the following objects:
## 1. The maximal cardinal for the proper subsets of interest. 
## 2. Between 1 and 2^dimension - 1 proper subsets of {1,...,d},
## represented by a matrix M with d columns.
## Each row represents a subset. The element M[i,j]
## is the jth element of the ith subset if j is less or equal to the
## cardinal of the ith subset, and 0 otherwise.  For instance if
## d = 3 and the chosen subsets are {3} and {1,2} then M is
## equal to
## 3 0 0
## 1 2 0.

## encoding: the way the subsets are represented. At the moment only
## "orthodox" is allowed, which corresponds to the way described above.

## The output is a list with two elements.
## - The first is a matrix R of size n * (s+1), where n is the size of the
## Monte Carlo sample and s is the number of chosen subsets. To fill R,
## the following is done Two independent input datasets are drawn. The
## first is passed to the model, which produces the outputs in the first
## column of R.  Let A be the jth subset of interest.  If x_i is the ith
## row of the first dataset and y_i is the ith row of the second dataset
## then a new vector z_i is made, where the kth component of z_i is that
## of x_i if k does not belong to A, and that of y_i otherwise.  That
## makes a new dataset which is passed on to the model to produce the
## outputs of the jth column of R.
## - The second is a matrix that represents the subsets of interest. If
## 'subsets' was a matrix already, it is kept as is. 


generateOutputs <- function(model, sizeMonteCarlo, keepIn=NULL, subsets=NULL, dimension){
    n <- sizeMonteCarlo
    d <- dimension
    subsets <- d
    b <- keepIn <- 1
    
    ## if 'subsets' is not a matrix, create it
    if(!is.matrix(subsets)){
        subsets <- makeAllSubsetsLessThanThreshold(1:d, subsets)
        subsets <- subsets[,(d+2):ncol(subsets)]
        dimnames(subsets) <- NULL
    } # else do nothing

    outputs <- matrix(NA, nrow=n, ncol=nrow(subsets)+1)
    X3 <- matrix(NA, nrow=n, ncol=d)

    ## change subsets representation
    orthodox <- subsets
    subsets <- t(apply(subsets, 1, orthodoxToBinary, A=1:d))

    ## generate two Monte Carlo datasets uniformly on [0,1]^d
    X1 <- matrix(runif(n*d), nrow=n, ncol=d)
    X2 <- matrix(runif(n*d), nrow=n, ncol=d)
    W <- matrix(sample(x=c(0,1), size=n*(2^d-1), replace=TRUE,
                       prob=c(1-b,b)), nrow=n, ncol=2^d-1)

    ## evaluate model according to pick-freeze design and store results
    for(i in 1:n){

        ## fill first column of outputs
        if(sum(W[i,]) >= 1){
            outputs[i,1] <- model(X1[i,])
        } # else nothing, since matrix 'outputs' already filled w/ NAs 

        ## fill remaining columns
        for(k in 1:nrow(subsets)){ # for each subset,

            ## if needed, 
            if(W[i,k] == 1){

                ## combine X1 and X2 by freezing the variables not in the subset
                X3[i,] <- X1[i,]
                for(j in 1:d){
                    if(subsets[k,j] != 0){
                        X3[i,j] <- X2[i,j]
                    }
                }
        
                ## apply model to this combination and store
                outputs[i,k+1] <- model(X3[i,])
            }
        }
    }
    list(outputs = outputs, subsets = orthodox)
}

## TEST 
## model <- function(x) pnorm(x[1]) + 2 * pnorm(x[2]) + pnorm(x[1]*x[2])
## (M <- matrix(c(3,1,2,0,0,0,0,0,0), nrow=3, ncol=3))
## generateOutputs(model = model, sizeMonteCarlo = 5, keepIn = 1, subsets = M, dimension = 2)
## generateOutputs(model = model, sizeMonteCarlo = 5, keepIn = 1, subsets = 1, dimension = 2)

#####################################
## ESTIMATE TOTAL SENSITIVITY INDICES 
#####################################

## The function 'inferenceTotal' estimate the total sensitivity indices
## associated with the subsets 'subsets' from the outputs 'outputs'
## (as returned by the function 'generateOutputs'). If the largest chosen subset
## has cardinal less than d then the classical estimate of the model output variance
## is also computed from the first column of 'outputs'.

## print the subsets
makeRowNames <- function(subsets){
    Amazing <- character(nrow(subsets))
    for(i in 1:nrow(subsets)){
        elements <- subsets[i, subsets[i,] != 0]
        amazing <- paste(elements[1])
        if(length(elements)>1){
            for(j in 2:length(elements)){
                amazing <- paste(amazing, paste(",",elements[j], sep=""), sep="")
            }
        }
        Amazing[i] <- amazing
    }
    Amazing
}

## main function
inferenceTotal <- function(outputs, subsets){
    stopifnot((ncol(outputs) - 1) == nrow(subsets))
    n <- nrow(outputs)
    d <- ncol(subsets)
    stopifnot(d>=2)
    nbSubsets <- ncol(outputs) - 1
    findID <- function(r){binaryToInteger(orthodoxToBinary(r, 1:d), 1:d)}
    
    ## find the subset's identifiers
    id <- apply(subsets, 1, findID)
    
    ## matrix to be returned
    T <- matrix(NA, nrow=nbSubsets, ncol=4, dimnames=list(
           makeRowNames(subsets), c("estimated.index", "standard.error",
                                    "p-value", "identifier")))
    
    ## fill the matrix and compute var.-cov. matrix of Gaussian limit
    tot <- matrix(NA, nrow=nrow(outputs), ncol=nrow(subsets))
    for(j in 1:nbSubsets){
        tot[,j] <- (outputs[,1] - outputs[,j+1])^2/2
        tothat <- mean(tot[,j])
        totstde <- sd(tot[,j])/sqrt(n)
        pvalue <- pnorm(tothat/totstde, lower.tail=FALSE)
        T[j,] <- c(tothat, totstde, pvalue, id[j])
    }
    Sigma <- cov(tot,tot)

    ## if subset {1,...,d} is missing,
      ## append classical estimator of variance, and
      ## compute var.-cov. of Gaussian limit
    missing <- sum(apply(subsets, 1, function(r){length(r[r!=0])==d}))==0
    if(missing){
        fx <- outputs[,1]
        variance <- var(fx)
        m <- mean(fx)
        Var <- 4 * m^2 * variance + var(fx^2) - 4 * m * cov(fx, fx^2)
        pvalue <- pnorm(variance/sqrt(Var/n), lower.tail=FALSE)
        T <- rbind(T, c(variance, sqrt(Var/n), pvalue, findID(1:d)))
        rn <- "1"
        for(i in 2:d) rn <- paste(rn,",",i,sep="")
        rownames(T)[nrow(T)] <- rn

        Sigma11 <- Sigma # upper-left, size (2^d-2)*(2^d-2)
        Sigma12 <- cov(tot, cbind(fx,fx^2)) # upper-right, (2^d-2)*2
        Sigma22 <- cov(cbind(fx,fx^2),cbind(fx,fx^2))
        avarcovTSIup <- cbind(Sigma11,Sigma12%*%c(-2*mean(fx),1))
        avarcovTSIdown <- cbind(c(-2*mean(fx),1)%*%t(Sigma12),
                                c(-2*mean(fx),1)%*%Sigma22%*%c(-2*mean(fx),1))
        Sigma <- rbind(avarcovTSIup,avarcovTSIdown)
    }

    ## return table and 
    list(indices=T, varcov=Sigma) 
}

## A list with two elements is returned.  The first is a matrix---the
## number of rows of which is equal to the number of computed estimates
## (equal to the number of subsets plus one if the largest chosen subset
## has cardinal less than d)--- with two columns: the first with the
## estimates and the second with the standard errors.  The second element
## of the list is 'subsets' itself, below which the subset {1,...,d} is
## concatenated if the largest chosen subset
## has cardinal less than d.

############################
## ESTIMATE SOBOL INDICES ##
############################

## totalIndices is as returned by the function 'inferenceTotal'

inferenceSobol <- function(totalIndices){ # model, sizeMonteCarlo, dimension

    ## extract basic information
    tsi <- totalIndices$indices
    Sigma_tsi <- totalIndices$varcov
    identifiers <- tsi[,"identifier"]
    d <- ceiling(nchar(rownames(tsi)[nrow(tsi)])/2)
    print(d)
    n <- Sigma_tsi[1,1]/(tsi[1,"standard.error"])^2

    ## transform the subsets to their orthodox form (result is a list)
    subsets <- sapply(tsi[,"identifier"],
                      function(s){binaryToOrthodox(integerToBinary(s, 1:d), 1:d)})

    ## detect whether a full analysis should be done
    doFullAnalysis <- nrow(tsi)==(2^d-1)

    if(doFullAnalysis){ ## FULL ANALYSIS

        ## var.-cov. matrix of the Gaussian limit
        Sigma_Sobol <- L <- matrix(NA, nrow=2^d-1, ncol=2^d-1)

        for(i in 1:length(subsets)){ # nonempty A in {1,...,d}
            A <- subsets[[i]]
            for(j in 1:length(subsets)){ # nonempty C in {1,...,d}
                C <- subsets[[j]]
                ## element at row A and column C
                summ <- 0
                for(ip in 1:length(subsets)){
                    B <- subsets[[ip]]
                    if(setequal(union(B,A),1:d)){ # (B or A) = {1,...,d}
                        summ <- summ + (-1)^(length(intersect(A,B))+1) *
                            Sigma_tsi[ip,j]
                    }
                }
                L[i,j] <- summ
            }
        }

        for(i in 1:length(subsets)){ # nonempty A in {1,...,d}
            A <- subsets[[i]]
            for(j in 1:length(subsets)){ # nonempty C in {1,...,d}
                C <- subsets[[j]]
                ## element at row A and column C
                summ <- 0
                for(ip in 1:length(subsets)){
                    B <- subsets[[ip]]
                    if(setequal(union(B,C),1:d)){ # (C or A) = {1,...,d}
                        summ <- summ + (-1)^(length(intersect(C,B))+1) *
                            L[i,ip]
                    }
                }
                Sigma_Sobol[i,j] <- summ
            }
        }
                        

        ## table to be returned
        V <- matrix(c(rep(NA, 3*(2^d-1)), identifiers), nrow=2^d-1, ncol=4,
                    dimnames=list(rownames(tsi),
                                  c("estimated.index", "standard.error", "p-value", "identifier")))

        ## Estimate Sobol index VA for every proper subset A
        for(i in 1:length(subsets)){ # for each nonempty subset A of {1,...,d} 

            VA <- 0

            ## find the subsets B of A
            A <- subsets[[i]]
            card <- length(A)
            SubSubsets <- makeAllSubsetsLessThanThreshold(A, card)
            dim(SubSubsets) <- c(2^card - 1, 1 + 2 * card)
            SubSubsets <- SubSubsets[, (card+2) : ncol(SubSubsets)]
            dim(SubSubsets) <- c(2^card - 1, card)

            ## for each B in A
            for(j in 1:nrow(SubSubsets)){
                
                ## find the complements
                B <- SubSubsets[j, SubSubsets[j,] != 0]
                idB <- binaryToInteger(orthodoxToBinary(B, 1:d), 1:d)
                
                if(idB==tsi[nrow(tsi),"identifier"]){
                    Bc <- idBc <- TBc <- 0 # emptyset case
                }else{
                    Bc <- setdiff(1:d, B)
                    idBc <- binaryToInteger(orthodoxToBinary(Bc, 1:d), 1:d)
                    TBc <- tsi[identifiers==idBc,1]
                }

                ## and the associated total index (Mobius inversion formula)
                if(idB == identifiers[i]){
                    card_A_minus_B <- 0
                }else{
                    card_A_minus_B <- length(setdiff(A, B))
                }
                VA <- VA + (-1)^(card_A_minus_B) * (tsi[nrow(tsi),1] - TBc)
            }
            
            sobstde <- sqrt(Sigma_Sobol[i,i]/n)
            pvalue <- pnorm(VA/sobstde, lower.tail=FALSE)
            V[i, 1:3] <- c(VA, sobstde, pvalue)
        }

    }else{ ## REDUCED ANALYSIS
        stop("NOT IMPLEMENTED YET")
    }
    
    ## return
    list(indices=V, varcov=Sigma_Sobol)
}

##############################
## ESTIMATE SHAPLEY INDICES ##
##############################

human <- function(id, d){
    B <- binaryToOrthodox(integerToBinary(id, 1:d), 1:d)
    B[B != 0]
}

inferenceShapley <- function(SobolIndices){

    V <- SobolIndices$indices
    Sigma_Sobol <- SobolIndices$varcov
    d <- ceiling(nchar(rownames(V)[nrow(V)])/2) # we assume that all of the 2^d-1
                                        # Sobol indices are present in V
    n <- Sigma_Sobol[1,1]/(V[1,"standard.error"])^2

    ## table to be returned
    Shap <- matrix(NA, nrow=d, ncol=4, dimnames=list(1:d, c("estimated.index",
                                    "standard.error", "pvalue", "identifier")))
        
    ## var.-cov. matrix of the Gaussian limit 
    Sigma_Shap <- matrix(NA, nrow=d, ncol=d)

    for(i in 1:d){ # for each set a={i}

        A <- i

        for(j in 1:d){ # for each set c={j}

            ## element at row A and column C
            C <- j
            summ <- 0
            
            ## for each nonempty subset B of {1,...,d} (again we assume that
                                        #  all of the 2^d-1
                                        # Sobol indices are present in V)
            for(ip in 1:nrow(V)){

                B <- human(V[ip,"identifier"], d)

                for(jp in 1:nrow(V)){
                    
                    Bp <- human(V[jp,"identifier"], d)

                    if((length(intersect(B,C))!=0) & # B and C != emptyset and
                       (length(intersect(Bp,A))!=0)){ # Bp and A != emptyset
                        summ <- summ +
                            (1/length(B)) * (1/length(Bp)) * Sigma_Sobol[jp,ip]
                    }
                }
            }
            Sigma_Shap[i,j] <- summ
        }
    }

    ## compute the index estimates
    for(i in 1:d){ # for each set a={i}

        a <- i
        ShapA <- 0

        ## for each nonempty subset B of {1,...,d} (again we assume that
                                        #  all of the 2^d-1
                                        # Sobol indices are present in V)
        for(j in 1:nrow(V)){

            b <- human(V[j,"identifier"], d)

            if(length(intersect(a,b)) != 0){
                ShapA <- ShapA + V[j,"estimated.index"]/length(b)
            }
        }

        shapstde <- sqrt(Sigma_Shap[i,i]/n)
        pvalue <- pnorm(ShapA/shapstde, lower.tail=FALSE)
        Shap[i,] <- c(ShapA, shapstde, pvalue, 
                         binaryToInteger(orthodoxToBinary(c(i,rep(0,d-1)), 1:d), 1:d))
    }
    
    Shap
}



## Make graphs (may be obsolete)
## Here the indices are those of the atomic partition, so that each of
## 'newComponents', 'Sob' and 'totalIndices' have d rows, where d is
## the number of inputs.

makeGraphs <- function(totalIndices, newComponents, Sob, variance, whichPlot){

    labels <- character(0)
    for(i in 1:nrow(newComponents)){
        labels <- c(labels, paste("input ", rownames(Sob)[i], sep=""))
    }

    if(whichPlot=="piePlot"){ # Pie chart? https://eagereyes.org/
                                        # criticism/in-defense-of-pie-charts
        ## or consider lessR::PieChart, see https://r-coder.com/pie-chart-r/

        pie(newComponents[,1], labels, radius=.8)
        segments(-1,-1,-1,1)
        segments(-1,1,1,1)
        segments(1,1,1,-1)
        segments(-1,-1,1,-1)
        dev.new()
        alpha <- sqrt( sum(abs(Sob[,1])) / variance ) * 0.8
        pie(x=abs(Sob[,1]), labels=labels, radius=alpha)
        segments(-1,-1,-1,1)
        segments(-1,1,1,1)
        segments(1,1,1,-1)
        segments(-1,-1,1,-1)
        
    }else if(whichPlot=="barPlot"){

        bp <- cbind(totalIndices[,1], newComponents[,1], Sob[,1])
        rownames(bp) <- labels
        colnames(bp) <- c("total", "new", "Sobol")
        stackedBars <- barplot(bp, legend.text=TRUE)
        segments(0,variance,3.5,variance,col="red",lwd=2)
        text(2, variance*(1.05), "variance", col="red")

        ## draw confidence intervals
        dev.new()
        adjacentBars <- barplot(bp, legend.text=TRUE, beside=TRUE)
        for(i in 1:3){
            if(i==1){
                theComponents <- totalIndices
            }else if(i==2){
                theComponents <- newComponents
            }else if(i==3){
                theComponents <- Sob
            }
            for(j in 1:nrow(Sob)){
                low <- theComponents[j,1] - 1.96 * theComponents[j,2]
                high <- theComponents[j,1] + 1.96 * theComponents[j,2]
                segments(adjacentBars[j,i], low, adjacentBars[j,i], high, lwd=4, col="blue")
            }
        }
    }
}







\name{gsigsa-package}
\alias{gsigsa-package}
\alias{gsigsa}
\docType{package}
\title{
\packageTitle{gsigsa}
}
\description{
\packageDescription{gsigsa}
}
\details{

The DESCRIPTION file:
\packageDESCRIPTION{gsigsa}
\packageIndices{gsigsa}
 %% An overview of how to use the package, including the most important 
 %% functions 
}
\author{
\packageAuthor{gsigsa}

Maintainer: \packageMaintainer{gsigsa}
}
%% \references{
%%  Literature or other references for background information 
%% }
%% ~~ Optionally other standard keywords, one per line, from file KEYWORDS in ~~
%% ~~ the R documentation directory ~~
\keyword{ package }
%% \seealso{
%%  Optional links to other man pages, e.g. 
%% %~~ \code{\link[<pkg>:<pkg>-package]{<pkg>}} ~~ ????
%% }
%% \examples{
%%  pnorm(5)
%% }

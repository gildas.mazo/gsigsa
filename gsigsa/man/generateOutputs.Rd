\name{generateOutputs}
\alias{generateOutputs}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Draw outputs from a given model
%%  ~~function to do ... ~~
}
\description{
Draw outputs from a given standardized model by sampling two
independent input vectors and evaluating the model at every vertex of
the hypercube passing through those two input vectors.
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
generateOutputs(model, sizeMonteCarlo, keepIn = NULL, subsets = NULL, dimension)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{model}{
the standardized model (see Details).
%%     ~~Describe \code{model} here~~
}
  \item{sizeMonteCarlo}{
size of the Monte Carlo experiment, that is, the number of input space explorations desired for statistical inference.
%%     ~~Describe \code{sizeMonteCarlo} here~~
}
  \item{keepIn}{
not used.
%%     ~~Describe \code{keepIn} here~~
}
  \item{subsets}{
not used.
%%     ~~Describe \code{subsets} here~~
}
  \item{dimension}{
number of inputs.
%%     ~~Describe \code{dimension} here~~
}
}
\details{
Denoting the two random input vectors for the ith exploration by 
X_i=(X_[i,1],...,X_[i,d]) and X'_i=(X'_[i,1],...,X'_[i,d]), and the model by f, 
the evaluations at the hypercube vertices are given by f(X^(-A)_i), where 
here A is a subset of (1,...,d) (the integers from 1 to d) and X^(-A)_i is
the vector X_i whose components corresponding to A have been replaced
by those of X'_i. For instance if d=4 and A=(3,1,4) then 
X^(-A)_i = (X'_[i,1],X_[i,2],X'_[i,3],X'_[i,4]).
It is assumed that the model has independent standard uniform inputs. 
(This can be achieved by composing the original model with the quantile 
functions of the input distributions, see the Example below.) 
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
a list with the following elements
 \item{outputs }{A matrix with 'sizeMonteCarlo' rows and 2^('dimension')
 columns of drawn outputs.}
 \item{subsets }{A matrix with 2^('dimension')-1 rows and 'dimension'
   columns, corresponding to all the elements of the powerset minus the
   emptyset, or, equivalently, all the subsets of 1,...,'dimension', or,
   equivalently again, all the vertices of the hypercube of dimension 
   'dimension'.}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Gildas Mazo
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
%% ##---- Should be DIRECTLY executable !! ----
%% ##-- ==>  Define data, use random,
%% ##--	or do  help(data=index)  for the standard data sets.
## Ishigami function, see Homma & Saltelli, 2001.
originalModel <- function(x, a = c(7, .1)){
    sin(x[1])+a[1]*sin(x[2])^2+a[2]*x[3]^4*sin(x[1])
}
## Rescale to Uniform(0,1) random variables
model <- function(x){
    originalModel(qunif(x, -pi, pi), a = c(7, .1))
}
generateOutputs(model = model, sizeMonteCarlo = 10, dimension = 3)

%% ## The function is currently defined as
%% function (model, sizeMonteCarlo, keepIn, subsets = 1, dimension, 
%%     encoding = "orthodox") 
%% {
%%     n <- sizeMonteCarlo
%%     d <- dimension
%%     b <- keepIn
%%     if (!is.matrix(subsets)) {
%%         subsets <- makeAllSubsetsLessThanThreshold(1:d, subsets)
%%         subsets <- subsets[, (d + 2):ncol(subsets)]
%%     }
%%     outputs <- matrix(NA, nrow = n, ncol = nrow(subsets) + 1)
%%     X3 <- matrix(NA, nrow = n, ncol = d)
%%     orthodox <- subsets
%%     subsets <- t(apply(subsets, 1, orthodoxToBinary, A = 1:d))
%%     X1 <- matrix(runif(n * d), nrow = n, ncol = d)
%%     X2 <- matrix(runif(n * d), nrow = n, ncol = d)
%%     W <- matrix(sample(x = c(0, 1), size = n * (2^d - 1), replace = TRUE, 
%%         prob = c(1 - b, b)), nrow = n, ncol = 2^d - 1)
%%     for (i in 1:n) {
%%         if (sum(W[i, ]) >= 1) {
%%             outputs[i, 1] <- model(X1[i, ])
%%         }
%%         for (k in 1:nrow(subsets)) {
%%             if (W[i, k] == 1) {
%%                 X3[i, ] <- X1[i, ]
%%                 for (j in 1:d) {
%%                   if (subsets[k, j] != 0) {
%%                     X3[i, j] <- X2[i, j]
%%                   }
%%                 }
%%                 outputs[i, k + 1] <- model(X3[i, ])
%%             }
%%         }
%%     }
%%     list(outputs = outputs, subsets = orthodox)
%%   }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
%% \keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
%% \keyword{ ~kwd2 }% __ONLY ONE__ keyword per line

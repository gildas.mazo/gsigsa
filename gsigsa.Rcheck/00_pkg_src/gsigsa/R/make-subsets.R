
## MAKE ALL SUBSETS OF SOME SET A (THERE ARE 2^|A| OF THEM)
###########################################################

## A: the set of which we want the subsets. Let n be its cardinal.
## The output is a matrix with 2^|A| rows and 1 + 2 * n columns.
## The first column is for the integer form, the second up to the (n+1)th
## columns are for the binary form and the (n+2)th up to the last columns
## are for the orthodox form.

makeAllSubsets <- function(A){
    n <- length(A)
    res <- matrix(nrow=2^n, ncol=1+n+n)
    dimnames(res) <- list(subset=1:2^n,
                          type=c("int", rep("b",n), rep("o",n)))

    for(i in 1:(2^n)){
        res[i,1] <- i - 1
        res[i,2:(n+1)] <- integerToBinary(res[i,1], A)
        tmp <- binaryToOrthodox(res[i,2:(n+1)], A)
        res[i,(n+2):(2*n+1)] <- c(tmp, rep(0,n-length(tmp)))
    }
    res
}

## TEST: make all subsets of{ 1,2,3,5 } (there are 16 of them)
## makeAllSubsets(c(1, 2, 3, 5)) # OK (as of 21 June 2021)

## MAKE ONLY THOSE SUBSETS WITH 0 < CARDINAL <= THRESHOLD
#########################################################

makeAllSubsetsLessThanThreshold <- function(A, threshold){     
    n <- length(A) 
    k <- threshold
    subsets <- makeAllSubsets(A)
    cardinal <- rowSums(as.matrix(subsets[,2:(n+1)]))
    subsets[which((0 < cardinal) & (cardinal <=k)), ]
}

## TEST: generate all subsets B of { 1,2,3,5 } s.t. 0 < |B| <= 2
## makeAllSubsetsLessThanThreshold(c(1, 2, 3, 5), 2)
## makeAllSubsetsLessThanThreshold(1, 1)

## COMPARE TWO SETS IN THE LEXICOGRAPHICAL ORDER 
################################################

## A < B? Must be in the standard form, e.g. (4,5)<(1,2,3)?

isLessThan <- function(A, B){ # A < B?
    n <- length(A) # cardinal of A
    m <- length(B) # cardinal of B
    if(n<m){
        out <- 1
    }else if(n>m){
        out <- 0
    }else{
        if(n==1){
            out <- as.numeric(A<B)
        }else{
            a <- A[length(A)]
            b <- B[length(B)]
            Am <- A[-length(A)]
            Bm <- B[-length(B)]
            ILT <- isLessThan(Am, Bm)
            SE <- setequal(Am, Bm)
            if(ILT | (SE & (a < b))){
                out <- 1
            }else{
                out <- 0
            }
        }
    }
    out
}

## SORT SUBSETS IN THE LEXICOGRAPHICAL ORDER
############################################

## naive, must be better algos

sortLexicographical <- function(subsets){
    d <- ncol(subsets)
    n <- nrow(subsets)
    orderedSubsets <- matrix(nrow=n, ncol=d)
    ranks <- numeric(length=n)
    for(i in 1:n){
        A <- subsets[i,]
        A <- A[A!=0] # rm zeros
        rankMinusOne <- sum(apply(subsets, 1,
                                  function(x){
                                      B <- x[x!=0]
                                      isLessThan(B,A)
                                  }))
        orderedSubsets[rankMinusOne + 1,] <- c(A, rep(0, d-length(A)))
        ranks[i] <- rankMinusOne + 1
    }
    list(orderedSubsets=orderedSubsets, ranks=ranks)
}

## Example
## subs <- makeAllSubsetsLessThanThreshold(1:3, 3)
## sortLexicographical(subs[,5:7])

The "gsigsa" package can be installed from the tar.gz archive file.
Launch a terminal, go to the same directory as that which the tar.gz
archive file was downloaded to, and type
> R CMD INSTALL gsigsa_1.0.tar.gz.

To see the main functions of the package, type
> package?gsigsa (or ?gsigsa).
Documentation for the functions is accessed as usual, that
is, type the name of the function right after a question mark.


